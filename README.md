# Rubik's Cube Solver

This small python script can visually identify a Rubik's Cube on webcam and calculate a solving method.
The solving method is based on the Herbert Kociemba's two-phase algorithm for solving Rubik's Cube

## Usage

T.B.D


## Author

Simon Mårtensson
